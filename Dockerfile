FROM python:3.8-alpine

LABEL maintainer="Alfred See <adchuenhong@gmail.com>"

ARG SSM_ENV_VERSION="0.0.3"

WORKDIR /root

RUN apk update && \
    apk --no-cache add ca-certificates && \
    wget -O /usr/local/bin/ssm-env https://github.com/remind101/ssm-env/releases/download/v${SSM_ENV_VERSION}/ssm-env && \
    echo 'da4bac1c1937da4689e49b01f1c85e28  /usr/local/bin/ssm-env' | md5sum -c && \
    chmod +x /usr/local/bin/ssm-env

COPY ./requirements.txt /root/requirements.txt

RUN pip install -r /root/requirements.txt

ENTRYPOINT [ "/usr/local/bin/ssm-env", "-with-decryption" ]
